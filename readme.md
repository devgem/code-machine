# Code machine

[![code-server](https://img.shields.io/badge/code--server-2.preview.11--vsc1.37.0-brightgreen.svg)](https://hub.docker.com/r/codercom/code-server/tags)
 [![vscode](https://img.shields.io/badge/vscode-1.37.0-brightgreen.svg)](https://github.com/microsoft/vscode/)
 [![dos2unix](https://img.shields.io/badge/dos2unix-7.3.4-brightgreen.svg)](http://dos2unix.sourceforge.net/)
 [![java](https://img.shields.io/badge/java-11.0.4-brightgreen.svg)](https://www.oracle.com/technetwork/java/javase/overview/index.html)
 [![netcore](https://img.shields.io/badge/netcore-2.2.401-brightgreen.svg)](https://dotnet.microsoft.com/download/dotnet-core)
 [![node](https://img.shields.io/badge/node-12.9.1-brightgreen.svg)](https://nodejs.org/)
 [![npm](https://img.shields.io/badge/npm-6.10.2-brightgreen.svg)](https://www.npmjs.com/)
 [![yarn](https://img.shields.io/badge/yarn-1.17.3-brightgreen.svg)](https://yarnpkg.com/)
 [![angular cli](https://img.shields.io/badge/ng--cli-8.3.2-brightgreen.svg)](https://cli.angular.io/)
 [![chromium](https://img.shields.io/badge/chromium-76.0.3809.100-brightgreen.svg)](https://www.chromium.org/Home)
 [![graphviz](https://img.shields.io/badge/graphviz-2.40.1-brightgreen.svg)](https://www.graphviz.org/)

[![gitlens](https://img.shields.io/badge/gitlens-9.9.3-brightgreen.svg)](https://github.com/eamodio/vscode-gitlens)
 [![omnisharp](https://img.shields.io/badge/omnisharp-1.21.1-brightgreen.svg)](https://github.com/OmniSharp/omnisharp-vscode)
 [![code-runner](https://img.shields.io/badge/code--runner-0.9.14-brightgreen.svg)](https://github.com/formulahendry/vscode-code-runner)
 [![csharp-xmldoc](https://img.shields.io/badge/csharp--xmldoc-0.1.8-brightgreen.svg)](https://github.com/kasecato/vscode-docomment)
 [![ng language service](https://img.shields.io/badge/ng--lang--svc-0.802.3-brightgreen.svg)](https://github.com/angular/vscode-ng-language-service)
 [![browser preview](https://img.shields.io/badge/browser--preview-0.5.9-brightgreen.svg)](https://github.com/angular/vscode-ng-language-service)
 [![chrome debugger](https://img.shields.io/badge/chrome--debugger-4.11.7-brightgreen.svg)](https://github.com/Microsoft/vscode-chrome-debug)
 [![plant uml](https://img.shields.io/badge/plantuml-2.12.0-brightgreen.svg)](https://github.com/qjebbs/vscode-plantuml)
 [![terminalis](https://img.shields.io/badge/terminalis-0.4.0-brightgreen.svg)](https://github.com/spectra-one/terminalis)
 [![ng inline](https://img.shields.io/badge/ng--lang--svc-0.0.17-brightgreen.svg)](https://github.com/natewallace/angular2-inline)

[![Docker Stars](https://img.shields.io/docker/stars/devgem/code-machine.svg)](https://hub.docker.com/r/devgem/code-machine 'DockerHub')
 [![Docker Pulls](https://img.shields.io/docker/pulls/devgem/code-machine.svg)](https://hub.docker.com/r/devgem/code-machine 'DockerHub')
 [![Docker Layers](https://images.microbadger.com/badges/image/devgem/code-machine.svg)](https://microbadger.com/images/devgem/code-machine)
 [![Docker Version](https://images.microbadger.com/badges/version/devgem/code-machine.svg)](https://hub.docker.com/r/devgem/code-machine)

## Overview

This docker image contains:

* [`code-server`](https://coder.com/) based on vscode
* [`vscode`](https://code.visualstudio.com/) with extensions:
  * [`gitlens`](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
  * [`omnisharp`](https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp)
  * [`code-runner`](https://marketplace.visualstudio.com/items?itemName=formulahendry.code-runner)
  * [`csharp-xmldoc`](https://marketplace.visualstudio.com/items?itemName=k--kato.docomment)
  * [`ng lang svc`](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template)
  * [`browser preview`](https://marketplace.visualstudio.com/items?itemName=auchenberg.vscode-browser-preview)
  * [`chrome debugger`](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome)
  * [`plant uml`](https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml)
  * [`terminalis`](https://marketplace.visualstudio.com/items?itemName=Spectra.terminalis)
  * [`ng inline`](https://marketplace.visualstudio.com/items?itemName=natewallace.angular2-inline)
* [`dos2unix`](http://dos2unix.sourceforge.net/)
* [`java`](https://www.oracle.com/technetwork/java/javase/overview/index.html)
* [`netcore sdk`](https://docs.microsoft.com/en-us/dotnet/core/sdk)
* [`node`](https://nodejs.org/)
* [`npm`](https://www.npmjs.com/)
* [`yarn`](https://yarnpkg.com/)
* [`angular cli`](https://cli.angular.io/)
* [`chromium`](https://www.chromium.org/Home)
* [`graphviz`](https://www.graphviz.org/)

To build the image:

```shell
export CODER_MACHINE_VERSION="0.7"
docker build -t devgem/code-machine:$CODER_MACHINE_VERSION --build-arg VERSION="$CODER_MACHINE_VERSION" --build-arg VCS_REF=`git rev-parse --short HEAD` --build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` .
```

### Usage

```shell
# on windows
mkdir c:/code-machine
docker run -d -p 8443:8443 -p 4200-4230:4200-4230 -p 5000-5030:5000-5030 -v c:/code-machine:/home/coder/project devgem/code-machine:0.7 --allow-http --no-auth --disable-telemetry

# on linux
cd ~
mkdir code-machine
docker run -d -p 8443:8443 -p 4200-4230:4200-4230 -p 5000-5030:5000-5030 -v code-machine:/home/coder/project devgem/code-machine:0.7 --allow-http --no-auth --disable-telemetry
```

### .NET Core

#### BOM

 See: <https://github.com/cdr/code-server/issues/597#issuecomment-490118664>

```shell
# when creating new projects with "dotnet new" make sure the files are saveable,
# a remove-bom script is availble to remove the BOM from all files in the current folder and subfolders recursively
remove-bom
```

#### Debugging

Since the debugger is only licensed for Microsofts official tooling, debugging .NET Core apps will not work (for now):

* <https://aka.ms/VSCode-DotNet-DbgLicense>
* <https://github.com/cdr/code-server/issues/473>

### Processes

```shell
# to check all running processes
pstree
# to get the pids for a processname, e.g. dotnet
pidof dotnet
# to get the pid of a process with a port, e.g. 5000
fuser 5000/tcp
# to kill a process for a pid, e.g. 1207
kill 1207
# to kill all processes for a processname, e.g. dotnet
kill $(pidof dotnet)
```

### Angular

#### Create an app

```shell
# to create an app using yarn
ng new test-project --skip-install
cd test-project
sudo yarn
```

#### Serve an app

```shell
# to serve an app that is available outside the container
npx ng serve --host 0.0.0.0
```

#### Debug an app

* From the menu choose `Debug > Add Configuration... > Browser Preview: Attach`
* Add the webroot of your project so the configuration looks like this (and replace `test-project` with the name of your project):

```json
        {
            "type": "browser-preview",
            "request": "attach",
            "name": "Browser Preview: Attach",
            "webRoot": "${workspaceFolder}/test-project"
        },
```

* Start serving your Angular app

```shell
npx ng serve --host 0.0.0.0
```

* Open a browser preview: `Ctrl+Shift+P` `View: Show Browser Preview`
* Surf to <http://localhost:4200>
* Go to the debug pane and choose configuration `Browser Preview: Attach`
* In the Debug toolbar click the debug "play" button
* If needed choose the tab with <http://localhost:4200>
* Happy debugging! :ant:

## Versions

Pre-built images are available on Docker Hub:

| image | date | code-server | vscode | dos2unix | gitlens | netcore | omnisharp | code-runner | csharp-xmldoc | ng lang svc | node | npm | yarn | ng | browser prev | chromium | chrome debugger | java | graphviz | plantuml | terminalis | ng inline |
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
| [`devgem/code-machine:0.7`](https://hub.docker.com/r/devgem/code-machine/tags/) | 20190903 | 2.preview.11-vsc1.37.0 | 1.37.0 | 7.3.4 | 9.9.3 | 2.2.401 | 1.21.1 | 0.9.14 | 0.1.8 | 0.802.3 | 12.9.1 | 6.10.2 | 1.17.3 | 8.3.2 | 0.5.9 | 76.0.3809.100 | 4.11.7 | 11.0.4 | 2.40.1 | 2.12.0 | 0.4.0 | 0.0.17 |
| [`devgem/code-machine:0.6`](https://hub.docker.com/r/devgem/code-machine/tags/) | 20190701 | 1.1156 | 1.33.1 | 7.3.4 | 9.8.2 | 2.2.300 | 1.20.0 | 0.9.11 | 0.1.8 | 0.800.0 | 12.5.0 | 6.9.0 | 1.16.0 | 8.0.6 | 0.5.9 | 73.0.3770.90 | 4.11.6 | 11.0.3 | 2.40.1 | 2.11.1 | 0.4.0 | 0.0.17 |
| [`devgem/code-machine:0.5`](https://hub.docker.com/r/devgem/code-machine/tags/) | 20190526 | 1.1119 | 1.33.1 | 7.3.4 | 9.8.1 | 2.2.300 | 1.19.1 | 0.9.9 | 0.1.7 | 0.1.11 | 12.3.1 | 6.9.0 | 1.16.0 | 7.3.9 | 0.5.7 | 73.0.3683.86 | 4.11.3 | 11.0.3 | 2.40.1 | 2.11.0 | 0.4.0 | NA |
| [`devgem/code-machine:0.4`](https://hub.docker.com/r/devgem/code-machine/tags/) | 20190512 | 1.939 | 1.33.1 | 7.3.4 | 9.7.1 | 2.2.203 | 1.19.1 | 0.9.9 | 0.1.6 | 0.1.11 | 12.2.0 | 6.9.0 | 1.16.0 | 7.3.9 | 0.5.4 | 73.0.3683.86 | 4.11.3 | 11.0.2 | 2.40.1 | 2.11.0 | NA | NA |
| [`devgem/code-machine:0.3`](https://hub.docker.com/r/devgem/code-machine/tags/) | 20190511 | 1.939 | 1.33.1 | 7.3.4 | 9.7.1 | 2.2.203 | 1.19.1 | 0.9.9 | 0.1.6 | 0.1.11 | 12.2.0 | 6.9.0 | 1.16.0 | 7.3.9 | 0.5.4 | 73.0.3683.86 | NA | NA | NA | NA | NA | NA |
| [`devgem/code-machine:0.2`](https://hub.docker.com/r/devgem/code-machine/tags/) | 20190507 | 1.939 | 1.33.1 | 7.3.4 | 9.7.1 | 2.2.203 | 1.19.1 | 0.9.8 | 0.1.6 | NA | 12.2.0 | 6.9.0 | NA | NA | NA | NA | NA | NA | NA | NA | NA | NA |
| [`devgem/code-machine:0.1`](https://hub.docker.com/r/devgem/code-machine/tags/) | 20190423 | 1.868 | 1.33.1 | NA | 9.6.3 | 2.2.203 | 1.19.0 | 0.9.8 | 0.1.6 | NA | NA | NA | NA | NA | NA | NA | NA | NA | NA | NA | NA | NA |

## Attributions

* Icon source: <https://github.com/Microsoft/vscode/issues/69803>
