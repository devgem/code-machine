FROM codercom/code-server:2.preview.11-vsc1.37.0

ARG VCS_REF
ARG VERSION
ARG BUILD_DATE

LABEL \
    org.label-schema.schema-version="1.0" \
    org.label-schema.name="code-machine" \
    org.label-schema.description="Based on code-server, the VSCode server, adds a bunch of devtools" \
    org.label-schema.vcs-url="https://gitlab.com/devgem/code-machine" \
    org.label-schema.vendor="DevGem" \
    org.label-schema.vcs-ref=$VCS_REF \
    version=$VERSION \
    build-date=$BUILD_DATE

# https://hub.docker.com/r/codercom/code-server/tags
ENV CODE_SERVER_VERSION 2.preview.11-vsc1.37.0
# https://github.com/cdr/code-server/releases
ENV VSCODE_VERSION 1.37.0
# https://github.com/cdr/code-server/blob/master/Dockerfile
ENV UBUNTU_VERSION 18.04
# https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens
ENV GITLENS_VERSION 9.9.3
# https://dotnet.microsoft.com/download/dotnet-core
ENV DOTNET_SDK_VERSION 2.2
# https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp
ENV OMNISHARP_VERSION 1.21.1
# https://marketplace.visualstudio.com/items?itemName=formulahendry.code-runner
ENV CODE_RUNNER_VERSION 0.9.14
# https://marketplace.visualstudio.com/items?itemName=k--kato.docomment
ENV CSHARP_XMLDOC_VERSION 0.1.8
# https://marketplace.visualstudio.com/items?itemName=Angular.ng-template
ENV ANGULAR_LANGUAGE_SERVICE_VERSION 0.802.3
# https://github.com/nodesource/distributions
ENV NODE_VERSION 12
# https://marketplace.visualstudio.com/items?itemName=auchenberg.vscode-browser-preview
ENV BROWSER_PREVIEW_VERSION 0.5.9
# https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome
ENV CHROME_DEBUGGER_VERSION 4.11.7
# https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml
ENV PLANTUML_VERSION 2.12.0
# https://marketplace.visualstudio.com/items?itemName=Spectra.terminalis
ENV TERMINALIS_VERSION 0.4.0
#https://marketplace.visualstudio.com/items?itemName=natewallace.angular2-inline
ENV ANGULAR_INLINE_TEMPLATE_VERSION 0.0.17

# Install dos2unix and system stuff
COPY scripts/remove-bom /usr/local/bin
RUN \
    sudo apt-get update && \
    sudo apt-get install apt-utils -y && \
    sudo apt-get install bsdtar -y && \
    sudo apt-get install psmisc -y && \
    sudo apt-get install dos2unix -y && \
    sudo apt-get install graphviz -y && \
    sudo chmod u+x /usr/local/bin/remove-bom

# Install Java
RUN \
    sudo apt-get update && \
    sudo apt-get upgrade -y && \
    sudo apt-get install default-jdk -y

# Install .NET Core SDK
RUN \
    wget -q -P ~/ https://packages.microsoft.com/config/ubuntu/$UBUNTU_VERSION/packages-microsoft-prod.deb && \
    sudo dpkg -i ~/packages-microsoft-prod.deb && \
    rm ~/packages-microsoft-prod.deb && \
    sudo apt-get update && \
    sudo apt-get install software-properties-common -y && \
    sudo add-apt-repository universe && \
    sudo apt-get install apt-transport-https -y && \
    sudo apt-get update && \
    sudo apt-get install dotnet-sdk-$DOTNET_SDK_VERSION -y

# Install NodeJS and npm
RUN \
    curl -sL https://deb.nodesource.com/setup_$NODE_VERSION.x | sudo -E bash - && \
    sudo apt-get install -y nodejs

# Install yarn
RUN \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list && \
    sudo apt update && \
    sudo apt install --no-install-recommends yarn -y

# Install Angular CLI
RUN \
    sudo npm install -g @angular/cli

# Install Chromium
RUN \
    sudo apt-get install chromium-browser -y && \
    sudo rm -rf /var/lib/apt/lists/*

# Install VSCode extensions
ENV VSCODE_EXTENSIONS "/home/coder/.local/share/code-server/extensions"

# Install Omnisharp extension
RUN \
    mkdir -p $VSCODE_EXTENSIONS/omnisharp-$OMNISHARP_VERSION && \
    wget --no-check-certificate --content-disposition --retry-connrefused --waitretry=120 --read-timeout=20 --timeout=15 -t 5 --output-document=extension.zip https://marketplace.visualstudio.com/_apis/public/gallery/publishers/ms-vscode/vsextensions/csharp/$OMNISHARP_VERSION/vspackage && \
    mkdir extension && \
    bsdtar -xvf extension.zip -C extension && \
    rm -rf extension.zip && \
    mv extension/extension/* $VSCODE_EXTENSIONS/omnisharp-$OMNISHARP_VERSION

# when downloading packages form the visual studio marketplace you sometimes get http code 429 "To many requests"
# waiting 90 seconds in between packages seems to work

# Install GitLens extension
RUN \
    sleep 120 && \
    mkdir -p $VSCODE_EXTENSIONS/gitlens-$GITLENS_VERSION && \
    wget --no-check-certificate --content-disposition --retry-connrefused --waitretry=120 --read-timeout=20 --timeout=15 -t 5 --output-document=extension.zip https://marketplace.visualstudio.com/_apis/public/gallery/publishers/eamodio/vsextensions/gitlens/$GITLENS_VERSION/vspackage && \
    mkdir extension && \
    bsdtar -xvf extension.zip -C extension && \
    rm -rf extension.zip && \
    mv extension/extension/* $VSCODE_EXTENSIONS/gitlens-$GITLENS_VERSION

# Install Code Runner extension
RUN \
    sleep 120 && \
    mkdir -p $VSCODE_EXTENSIONS/code-runner-$CODE_RUNNER_VERSION && \
    wget --no-check-certificate --content-disposition --retry-connrefused --waitretry=120 --read-timeout=20 --timeout=15 -t 5 --output-document=extension.zip https://marketplace.visualstudio.com/_apis/public/gallery/publishers/formulahendry/vsextensions/code-runner/$CODE_RUNNER_VERSION/vspackage && \
    mkdir extension && \
    bsdtar -xvf extension.zip -C extension && \
    rm -rf extension.zip && \
    mv extension/extension/* $VSCODE_EXTENSIONS/code-runner-$CODE_RUNNER_VERSION

# Install C# XLM Documentation extension
RUN \
    sleep 120 && \
    mkdir -p $VSCODE_EXTENSIONS/csharp-xml-doc-$CSHARP_XMLDOC_VERSION && \
    wget --no-check-certificate --content-disposition --retry-connrefused --waitretry=120 --read-timeout=20 --timeout=15 -t 5 --output-document=extension.zip https://marketplace.visualstudio.com/_apis/public/gallery/publishers/k--kato/vsextensions/docomment/$CSHARP_XMLDOC_VERSION/vspackage && \
    mkdir extension && \
    bsdtar -xvf extension.zip -C extension && \
    rm -rf extension.zip && \
    mv extension/extension/* $VSCODE_EXTENSIONS/csharp-xml-doc-$CSHARP_XMLDOC_VERSION

# Install Angular Language Service extension
RUN \
    sleep 120 && \
    mkdir -p $VSCODE_EXTENSIONS/ng-lang-svc-$ANGULAR_LANGUAGE_SERVICE_VERSION && \
    wget --no-check-certificate --content-disposition --retry-connrefused --waitretry=120 --read-timeout=20 --timeout=15 -t 5 --output-document=extension.zip https://marketplace.visualstudio.com/_apis/public/gallery/publishers/Angular/vsextensions/ng-template/$ANGULAR_LANGUAGE_SERVICE_VERSION/vspackage && \
    mkdir extension && \
    bsdtar -xvf extension.zip -C extension && \
    rm -rf extension.zip && \
    mv extension/extension/* $VSCODE_EXTENSIONS/ng-lang-svc-$ANGULAR_LANGUAGE_SERVICE_VERSION

# Install browser preview extension
RUN \
    sleep 120 && \
    mkdir -p $VSCODE_EXTENSIONS/browser-preview-$BROWSER_PREVIEW_VERSION && \
    wget --no-check-certificate --content-disposition --retry-connrefused --waitretry=120 --read-timeout=20 --timeout=15 -t 5 --output-document=extension.zip https://marketplace.visualstudio.com/_apis/public/gallery/publishers/auchenberg/vsextensions/vscode-browser-preview/$BROWSER_PREVIEW_VERSION/vspackage && \
    mkdir extension && \
    bsdtar -xvf extension.zip -C extension && \
    rm -rf extension.zip && \
    mv extension/extension/* $VSCODE_EXTENSIONS/browser-preview-$BROWSER_PREVIEW_VERSION

# Install chrome debugger extension
RUN \
    sleep 120 && \
    mkdir -p $VSCODE_EXTENSIONS/chrome-debugger-$CHROME_DEBUGGER_VERSION && \
    wget --no-check-certificate --content-disposition --retry-connrefused --waitretry=120 --read-timeout=20 --timeout=15 -t 5 --output-document=extension.zip https://marketplace.visualstudio.com/_apis/public/gallery/publishers/msjsdiag/vsextensions/debugger-for-chrome/$CHROME_DEBUGGER_VERSION/vspackage && \
    mkdir extension && \
    bsdtar -xvf extension.zip -C extension && \
    rm -rf extension.zip && \
    mv extension/extension/* $VSCODE_EXTENSIONS/chrome-debugger-$CHROME_DEBUGGER_VERSION

# Install Plant UML extension
RUN \
    sleep 120 && \
    mkdir -p $VSCODE_EXTENSIONS/plantuml-$PLANTUML_VERSION && \
    wget --no-check-certificate --content-disposition --retry-connrefused --waitretry=120 --read-timeout=20 --timeout=15 -t 5 --output-document=extension.zip https://marketplace.visualstudio.com/_apis/public/gallery/publishers/jebbs/vsextensions/plantuml/$PLANTUML_VERSION/vspackage && \
    mkdir extension && \
    bsdtar -xvf extension.zip -C extension && \
    rm -rf extension.zip && \
    mv extension/extension/* $VSCODE_EXTENSIONS/plantuml-$PLANTUML_VERSION

# Install Terminalis extension
RUN \
    sleep 120 && \
    mkdir -p $VSCODE_EXTENSIONS/terminalis-$TERMINALIS_VERSION && \
    wget --no-check-certificate --content-disposition --retry-connrefused --waitretry=120 --read-timeout=20 --timeout=15 -t 5 --output-document=extension.zip https://marketplace.visualstudio.com/_apis/public/gallery/publishers/Spectra/vsextensions/terminalis/$TERMINALIS_VERSION/vspackage && \
    mkdir extension && \
    bsdtar -xvf extension.zip -C extension && \
    rm -rf extension.zip && \
    mv extension/extension/* $VSCODE_EXTENSIONS/terminalis-$TERMINALIS_VERSION

# Install Angular inline template extension
RUN \
    sleep 120 && \
    mkdir -p $VSCODE_EXTENSIONS/ng-inline-$ANGULAR_INLINE_TEMPLATE_VERSION && \
    wget --no-check-certificate --content-disposition --retry-connrefused --waitretry=120 --read-timeout=20 --timeout=15 -t 5 --output-document=extension.zip https://marketplace.visualstudio.com/_apis/public/gallery/publishers/natewallace/vsextensions/angular2-inline/$ANGULAR_INLINE_TEMPLATE_VERSION/vspackage && \
    mkdir extension && \
    bsdtar -xvf extension.zip -C extension && \
    rm -rf extension.zip && \
    mv extension/extension/* $VSCODE_EXTENSIONS/ng-inline-$ANGULAR_INLINE_TEMPLATE_VERSION

# Display versions
RUN \
    echo '' && \
    echo '********' && \
    echo 'VERSIONS' && \
    echo '********' && \
    echo '' && \
    echo 'ubuntu:' && \
    lsb_release -a && \
    echo '' && \
    echo 'code-server:' && \
    echo $CODE_SERVER_VERSION && \
    echo '' && \
    echo 'vscode:' && \
    echo $VSCODE_VERSION && \
    echo '' && \
    echo 'dos2unix:' && \
    dos2unix --version && \
    echo '' && \
    echo 'gitlens:' && \
    echo $GITLENS_VERSION && \
    echo '' && \
    echo 'netcore:' && \
    dotnet --version && \
    echo '' && \
    echo 'omnisharp:' && \
    echo $OMNISHARP_VERSION && \
    echo '' && \
    echo 'code-runner:' && \
    echo $CODE_RUNNER_VERSION && \
    echo '' && \
    echo 'csharp-xmlldoc:' && \
    echo $CSHARP_XMLDOC_VERSION && \
    echo '' && \
    echo 'angular language service:' && \
    echo $ANGULAR_LANGUAGE_SERVICE_VERSION && \
    echo '' && \
    echo 'node:' && \
    node --version && \
    echo '' && \
    echo 'npm:' && \
    npm --version && \
    echo '' && \
    echo 'yarn:' && \
    yarn --version && \
    echo '' && \
    echo 'angular cli:' && \
    ng --version && \
    echo '' && \
    echo 'browser preview:' && \
    echo $BROWSER_PREVIEW_VERSION && \
    echo '' && \
    echo 'chromium:' && \
    chromium-browser --product-version && \
    echo '' && \
    echo 'chrome debugger:' && \
    echo $CHROME_DEBUGGER_VERSION && \
    echo '' && \
    echo 'java:' && \
    java -version && \
    echo '' && \
    echo 'graphviz:' && \
    dot -V && \
    echo '' && \
    echo 'plantuml:' && \
    echo $PLANTUML_VERSION && \
    echo '' && \
    echo 'terminalis:' && \
    echo $TERMINALIS_VERSION && \
    echo '' && \
    echo 'angular inline template:' && \
    echo $ANGULAR_INLINE_TEMPLATE_VERSION && \
    echo '' && \
    echo '********' && \
    echo ''
